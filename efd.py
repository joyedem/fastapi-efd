from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
from enum import Enum
# req = {
#     "API_VERSION": "1.0.2"
#     }

class ModelInfo(str, Enum):
    MODEL_TAG = ""
    PREDICTIVE_MODEL_THRESHOLD = "0.011"
    PREDICTIVE_MODEL_VERSION = "1.0.2"
    PREDICTIVE_MODEL_VERSION_ID = "id1.0.2"

efd_app = FastAPI()

@efd_app.get("/getEFDScore")
def response():
    #return resp <- constructResp(body request)
#     efd_initializeStructure(req)
#   # Extract required fields
#   feature_extraction(req)
#   feature_transformation(req)
#   getModelPrediction(req)
#   getModelScore(req)
#   #Get features for watchlist
#   feature_transformation_watchlist(req)
#   #Read watchlist
#   get_watchlist(req)
#   #Compute watchlist action score rule
#   getWatchList_score_action_rule(req)
#   #Compute business conditions action score rule
#   getBusinessRules_score_action_rule(req)
#   #Compute final score action rule
#   getFinal_score_action_rule(req)
#   response = constructRes(req)
#   logInfo(req,res)
#   return(response)
    return {"response"}


@efd_app.get("/getVersions")
def versions():
    res = {
        "modelVersion": ModelInfo.PREDICTIVE_MODEL_VERSION,
        "modelVersionId": ModelInfo.PREDICTIVE_MODEL_VERSION_ID,
        "tag": ModelInfo.MODEL_TAG,
        "efd_threshold": ModelInfo.PREDICTIVE_MODEL_THRESHOLD     
     }
    return res
