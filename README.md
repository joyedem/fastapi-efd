# Learning About FastAPI

## Path Parameters
- these are variables declared with python syntax 
```
@app.get("/items/{item_id}")
def read_item(item_id: int):
    return {"item_id": item_id}
```
`item_id` is expected to be an integer, and converted to one when the API is run. 
response = `{"item_id":3}`

When you do not input an integer (ex: /items/foo), fastapi displays a useful error message about wrong typing 

### Order matters

When creating path operations, ensure the proper ordering so that paths are declared without errors. Be careful with dynamic paths such as /foo/{variable}. Ensure that fixed path operations are declared prior to dynamic path operations. 

## Predefined values
If you have a path operation that receives a path parameter, but you want the possible valid path parameter values to be predefined, you can use a standard Python Enum.
**Enum**: An enumeration is a set of symbolic names (members) bound to unique, constant values. 

### Create an Enum class
1. Import Enum and create a sub-class that inherits from str and from Enum.
- inheriting from str allows the API to expect a string and render correctly.
2. Create class attributes with fixed values, which will be the available valid values:

```
from enum import Enum
from fastapi import FastAPI

class ModelName(str, Enum):
    ALEXNET = "alexnet"
    RESNET = "resnet"
    LENET = "lenet"
```

Can apply this to EFD to store constants such as model tag and version:
```
class ModelInfo(str, Enum):
    API_VERSION = "1.0.2"
    MODEL_TAG = "efd_v2"
    MODEL_DEPLOYMENT_DATE = "21 AUG 2020"
```

*!Question to answer: how can we log api time with FastAPI? meaning API response time, is it stored automatically somewhere?!*

To access the value of the Enum: EnumClass.CONSTANT
